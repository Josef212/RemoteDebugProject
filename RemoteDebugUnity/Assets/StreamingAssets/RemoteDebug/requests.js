const RUN_COMMAND = "console/run?command=";
const GET_OUTPUT = "console/out";
const COMPLETE_COMMAND = "console/complete?command=";
const GET_COMMAND_LIST = "console/commandList";
const GET_COMMAND_HISTORY = "console/commandHistory";
const GET_COMMAND_HISTORY_INDEX = "console/commandHistory?index=";

const runCommand = command => {
    scrollToBottom();
    const url = `${RUN_COMMAND}${encodeURI(encodeURIComponent(command))}`;
    $.get(url, (data, status) => {
        updateConsole(() => {
            updateCommand(commandIndex - 1);
        });
    });
}

const updateConsole = cbk => {
    const url = GET_OUTPUT;
    $.get(url, (data, status) => {
        const shouldScroll = Math.abs(($output[0].scrollHeight - $output.scrollTop()) - $output.innerHeight()) < 5;
        $output.html(String(data).replace(/\n|\r/g, '<br>') + "<br><br><br>");
        if (cbk)
            cbk();

        if (shouldScroll)
            scrollToBottom();
    });
}

const updateCommandRequest = index => {
    const url = `${GET_COMMAND_HISTORY_INDEX}${index}`;
    $.get(url, (data, status) => {
        if (data) {
            commandIndex = index;
            $input.val(String(data));
        }
    });
}

const completeCommand = command => {
    const url = `${COMPLETE_COMMAND}${command}`;
    $.get(url, (data, status) => {
        if (data) {
            updateConsole();
            $input.val(String(data));
        }
    });
}