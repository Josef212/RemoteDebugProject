using UnityEngine;

public class UITestScene : MonoBehaviour
{
	public void OnLog()
	{
		Debug.Log("### Testing info log from UI button!");
	}

	public void OnError()
	{
		Debug.LogError("### Testing error log from UI button!");
	}
}
