using System.Collections;
using RemoteDebug.Attributes;
using UnityEngine;

namespace DefaultNamespace
{
	public class CounterTest : MonoBehaviour
	{
		private float _counter;
		private static Coroutine _coroutine;
		private static CounterTest _runner;
		
		private void Start()
		{
			_runner = this;
			StartCounter();
		}

		private IEnumerator Counter()
		{
			_counter = 0;
			while (true)
			{
				Debug.Log($"Counter: {_counter++}");
				yield return new WaitForSeconds(1f);
			}
		}

		[Command("counter.stop", "Stops counter", true)]
		private static void StopCounter()
		{
			if (_coroutine != null)
			{
				_runner.StopCoroutine(_coroutine);
				_coroutine = null;
			}
		}

		[Command("counter.start", "Starts counter", true)]
		private static void StartCounter()
		{
			StopCounter();
			_coroutine = _runner.StartCoroutine(_runner.Counter());
		}

		[Command("counter.reset", "Resets counter", true)]
		private static void ResetCounter()
		{
			_runner._counter = 0;
		}
	}
}