using RemoteDebug;
using RemoteDebug.Attributes;
using UnityEngine;
using Console = RemoteDebug.Console.Console;

namespace DefaultNamespace
{
	public class RotatorTest : MonoBehaviour
	{
		private static RotatorTest Instance;
		
		[SerializeField] private Transform _mesh;
		[SerializeField] private Vector3 _rotationSpeed = new Vector3(20f, 10f, 30f);

		private void Awake()
		{
			Instance = this;
		}

		private void OnDestroy()
		{
			Instance = null;
		}

		private void Update()
		{
			if (_mesh == null)
				return;

			var rotS = _rotationSpeed * Time.deltaTime;
			_mesh.Rotate(rotS);
		}

		[Command("rotator.setSpeed", "Sets rotator rotation speed")]
		public static void SetSpeed(Console console, string[] args)
		{
			if (Instance == null)
			{
				console.Warn("Rotator instance does not exist");
				return;
			}

			var rotSpeed = Instance._rotationSpeed;
			
			if (args.GetArgAt<float>(0, out var x))
				rotSpeed.x = x;
			if (args.GetArgAt<float>(1, out var y))
				rotSpeed.y = y;
			if (args.GetArgAt<float>(2, out var z))
				rotSpeed.z = z;

			Instance._rotationSpeed = rotSpeed;
		}

		[Command("rotator.getSpeed", "Prints rotator rotation speed")]
		public static void GetRotationSpeed(Console console)
		{
			if (Instance == null)
			{
				console.Warn("Rotator instance does not exist");
				return;
			}
			
			console.Info($"Rotator rotation speed: {Instance._rotationSpeed}");
		}

		[Command("rotator.start", "Start rotation")]
		public static void StartRotation()
		{
			if (Instance != null)
			{
				Instance.enabled = true;
			}
		}
		
		[Command("rotator.stop", "Stop rotation")]
		public static void StopRotation()
		{
			if (Instance != null)
			{
				Instance.enabled = false;
			}
		}
	}
}