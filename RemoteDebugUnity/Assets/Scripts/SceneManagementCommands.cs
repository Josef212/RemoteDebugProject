using System;
using System.Text;
using RemoteDebug;
using RemoteDebug.Attributes;
using UnityEngine.SceneManagement;
using Console = RemoteDebug.Console.Console;

namespace DefaultNamespace
{
	public static class SceneManagementCommands
	{
		[Command("scenes.list", "Lists all scenes in the build")]
		public static void ListScenes(Console console)
		{
			var sb = new StringBuilder();
			var count = SceneManager.sceneCountInBuildSettings;

			sb.AppendLine($"Scene count: {count} - Loaded scenes: {SceneManager.sceneCount}");

			for (var i = 0; i < count; i++)
			{
				var scene = SceneManager.GetSceneByBuildIndex(i);
				var name = scene.name;
				var path = scene.path;
				var index = scene.buildIndex;
				var loaded = scene.isLoaded;
				var rootCount = scene.rootCount;
				var subScene = scene.isSubScene;
				var dirty = scene.isDirty;

				sb.AppendLine($"  {name} - Path: {path} (Index: {index})\n\tLoaded: {loaded} RootCount: {rootCount} IsSubscene: {subScene} IsDirty: {dirty}");
			}
			
			console.Help(sb.ToString());
		}

		[Command("scenes.active", "Prints active scene")]
		public static void GetActiveScene(Console console)
		{
			var scene = SceneManager.GetActiveScene();
			console.Info($"Active scene: {scene.name}");
		}

		[Command("scenes.load", "Loads an scene by its name (-n) or index (-i). Add -a to load it in additive mode")]
		public static void LoadScene(Console console, string[] args)
		{
			var mode = args.GetFlag("-a") ? LoadSceneMode.Additive : LoadSceneMode.Single;
			
			if (args.GetArgValue<int>("-i", out var index))
			{
				if (index < 0 || index >= SceneManager.sceneCountInBuildSettings)
				{
					console.Error($"Index {index} is out of bounds");
					return;
				}

				SceneManager.LoadSceneAsync(index, mode);
				return;
			}
			else if (args.GetArgValue<string>("-n", out var name))
			{
				try
				{
					SceneManager.LoadSceneAsync(name, mode);
					return;
				}
				catch (Exception)
				{
					console.Error($"Could not find scene named {name}");
					return;
				}
			}
			
			console.Warn("Could not find specified scene");
		}

		[Command("scenes.unload", "Unloads specified scene by its name")]
		public static void UnloadScene(Console console, string[] args)
		{
			if (!args.GetArgAt<string>(0, out var name))
			{
				console.Error($"Invalid command arguments. Expecting a scene name.");
				return;
			}

			try
			{
				var scene = SceneManager.GetSceneByName(name);
				console.Info($"Unloading scene [{scene.name}]");
				SceneManager.UnloadSceneAsync(scene);
			}
			catch (Exception)
			{
				// ignored
			}
		}
	}
}