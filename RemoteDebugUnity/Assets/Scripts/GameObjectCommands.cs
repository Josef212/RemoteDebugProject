using System.Text;
using RemoteDebug;
using RemoteDebug.Attributes;
using RemoteDebug.Console;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
	public static class GameObjectCommands
	{
		[Command("object.printRoot", "Prints root game objects of the active scenes")]
		public static void PrintRoot(Console console)
		{
			var sb = new StringBuilder();

			var roots = SceneManager.GetActiveScene().GetRootGameObjects();
			sb.Append($"Root game objects ({roots.Length})");
			for (var i = 0; i < roots.Length; i++)
			{
				var go = roots[i];
				sb.Append($"\n  Go[{i}]: {go.name} - Enable:{go.activeSelf} - (C:{go.transform.childCount})");
			}
			
			console.Log(sb.ToString());
		}

		[Command("object.printHierarchy",
			"Prints the full scene hierarchy of the active scene. NOTE that this might have a big impact on big scenes")]
		public static void PrintHierarchy(Console console)
		{
			var sb = new StringBuilder();

			var roots = SceneManager.GetActiveScene().GetRootGameObjects();
			for (var i = 0; i < roots.Length; i++)
			{
				var go = roots[i];
				sb.Append($"[{0}]: {go.name} - E:{go.activeSelf} (C:{go.transform.childCount})\n");
				PrintGoHierarchy(sb, go, 1);
			}
			
			console.Log(sb.ToString());
		}

		[Command("object.setActive", "Set game object active")]
		public static void SetGoActive(Console console, string[] args)
		{
			if (!args.GetArgAt<string>(0, out var name))
			{
				console.Warn($"Could not get game object name: {name}");
				return;
			}

			var go = GameObject.Find(name);
			if (go == null)
			{
				console.Warn($"Could not get game object: {name}");
				return;
			}

			var targetState = !go.activeSelf;
			if (args.GetArgAt<bool>(1, out var state))
			{
				targetState = state;
			}
			
			go.SetActive(targetState);
			console.Info($"GameObject changed state to {targetState}.");
		}

		[Command("object.printGoChild", "Prints a game object child hierarchy")]
		public static void PrintGoChild(Console console, string[] args)
		{
			if (!args.GetArgAt<string>(0, out var name))
			{
				console.Warn($"Could not get game object name: {name}");
				return;
			}
			
			var go = GameObject.Find(name);
			if (go == null)
			{
				console.Warn($"Could not get game object: {name}");
				return;
			}

			var sb = new StringBuilder($"GO [{name}] hierarchy:\n");
			sb.Append($"[{0}]: {go.name} - E:{go.activeSelf} (C:{go.transform.childCount})\n");
			PrintGoHierarchy(sb, go, 1);
			
			console.Info(sb.ToString());
		}

		private static void PrintGoHierarchy(StringBuilder sb, GameObject go, int indent, bool isLastChild = false)
		{
			const char line = '│';
			const char vertical = '├';
			const char corner = '└';
			const char horizontal = '─';
		
			if (go == null)
			{
				return;
			}
			
			var transform = go.transform;

			var count = transform.childCount;
			var indentCount = indent * 2 - 1;
			for (var i = 0; i < count; i++)
			{
				for (var j = 0; j < indentCount; j++)
					sb.Append(j % 2 == 0 || isLastChild ? ' ' : line);
				
				sb.Append(i == count - 1 ? corner : vertical);
				sb.Append($"[{indent}]: {go.name} - E:{go.activeSelf} (C:{go.transform.childCount})\n");
				
				PrintGoHierarchy(sb, transform.GetChild(i).gameObject, indent + 1, i == count - 1);
			}
		}
	}
}